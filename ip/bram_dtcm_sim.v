// Verilog netlist created by TD v5.0.28716
// Wed Apr  7 08:18:28 2021

`timescale 1ns / 1ps
module bram_dtcm  // bram_dtcm.v(14)
  (
  addra,
  cea,
  clka,
  dia,
  wea,
  doa
  );

  input [13:0] addra;  // bram_dtcm.v(29)
  input cea;  // bram_dtcm.v(31)
  input clka;  // bram_dtcm.v(32)
  input [31:0] dia;  // bram_dtcm.v(28)
  input [3:0] wea;  // bram_dtcm.v(30)
  output [31:0] doa;  // bram_dtcm.v(26)

  parameter ADDR_WIDTH_A = 14;
  parameter ADDR_WIDTH_B = 14;
  parameter DATA_DEPTH_A = 16384;
  parameter DATA_DEPTH_B = 16384;
  parameter DATA_WIDTH_A = 32;
  parameter DATA_WIDTH_B = 32;
  parameter REGMODE_A = "NOREG";
  parameter WRITEMODE_A = "NORMAL";
  wire [0:0] addra_piped;
  wire inst_doa_i0_000;
  wire inst_doa_i0_001;
  wire inst_doa_i0_002;
  wire inst_doa_i0_003;
  wire inst_doa_i0_004;
  wire inst_doa_i0_005;
  wire inst_doa_i0_006;
  wire inst_doa_i0_007;
  wire inst_doa_i0_008;
  wire inst_doa_i0_009;
  wire inst_doa_i0_010;
  wire inst_doa_i0_011;
  wire inst_doa_i0_012;
  wire inst_doa_i0_013;
  wire inst_doa_i0_014;
  wire inst_doa_i0_015;
  wire inst_doa_i0_016;
  wire inst_doa_i0_017;
  wire inst_doa_i0_018;
  wire inst_doa_i0_019;
  wire inst_doa_i0_020;
  wire inst_doa_i0_021;
  wire inst_doa_i0_022;
  wire inst_doa_i0_023;
  wire inst_doa_i0_024;
  wire inst_doa_i0_025;
  wire inst_doa_i0_026;
  wire inst_doa_i0_027;
  wire inst_doa_i0_028;
  wire inst_doa_i0_029;
  wire inst_doa_i0_030;
  wire inst_doa_i0_031;
  wire inst_doa_i1_000;
  wire inst_doa_i1_001;
  wire inst_doa_i1_002;
  wire inst_doa_i1_003;
  wire inst_doa_i1_004;
  wire inst_doa_i1_005;
  wire inst_doa_i1_006;
  wire inst_doa_i1_007;
  wire inst_doa_i1_008;
  wire inst_doa_i1_009;
  wire inst_doa_i1_010;
  wire inst_doa_i1_011;
  wire inst_doa_i1_012;
  wire inst_doa_i1_013;
  wire inst_doa_i1_014;
  wire inst_doa_i1_015;
  wire inst_doa_i1_016;
  wire inst_doa_i1_017;
  wire inst_doa_i1_018;
  wire inst_doa_i1_019;
  wire inst_doa_i1_020;
  wire inst_doa_i1_021;
  wire inst_doa_i1_022;
  wire inst_doa_i1_023;
  wire inst_doa_i1_024;
  wire inst_doa_i1_025;
  wire inst_doa_i1_026;
  wire inst_doa_i1_027;
  wire inst_doa_i1_028;
  wire inst_doa_i1_029;
  wire inst_doa_i1_030;
  wire inst_doa_i1_031;

  AL_DFF_X addra_pipe (
    .ar(1'b0),
    .as(1'b0),
    .clk(clka),
    .d(addra[13]),
    .en(cea),
    .sr(1'b0),
    .ss(1'b0),
    .q(addra_piped));
  EG_PHY_CONFIG #(
    .DONE_PERSISTN("ENABLE"),
    .INIT_PERSISTN("ENABLE"),
    .JTAG_PERSISTN("DISABLE"),
    .PROGRAMN_PERSISTN("DISABLE"))
    config_inst ();
  // address_offset=0;data_offset=0;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_000 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n62,open_n63,addra[13]}),
    .dia({open_n67,open_n68,open_n69,open_n70,open_n71,open_n72,open_n73,dia[0],open_n74}),
    .wea(wea[0]),
    .doa({open_n89,open_n90,open_n91,open_n92,open_n93,open_n94,open_n95,open_n96,inst_doa_i0_000}));
  // address_offset=0;data_offset=1;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_001 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n121,open_n122,addra[13]}),
    .dia({open_n126,open_n127,open_n128,open_n129,open_n130,open_n131,open_n132,dia[1],open_n133}),
    .wea(wea[0]),
    .doa({open_n148,open_n149,open_n150,open_n151,open_n152,open_n153,open_n154,open_n155,inst_doa_i0_001}));
  // address_offset=0;data_offset=2;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_002 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n180,open_n181,addra[13]}),
    .dia({open_n185,open_n186,open_n187,open_n188,open_n189,open_n190,open_n191,dia[2],open_n192}),
    .wea(wea[0]),
    .doa({open_n207,open_n208,open_n209,open_n210,open_n211,open_n212,open_n213,open_n214,inst_doa_i0_002}));
  // address_offset=0;data_offset=3;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_003 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n239,open_n240,addra[13]}),
    .dia({open_n244,open_n245,open_n246,open_n247,open_n248,open_n249,open_n250,dia[3],open_n251}),
    .wea(wea[0]),
    .doa({open_n266,open_n267,open_n268,open_n269,open_n270,open_n271,open_n272,open_n273,inst_doa_i0_003}));
  // address_offset=0;data_offset=4;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_004 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n298,open_n299,addra[13]}),
    .dia({open_n303,open_n304,open_n305,open_n306,open_n307,open_n308,open_n309,dia[4],open_n310}),
    .wea(wea[0]),
    .doa({open_n325,open_n326,open_n327,open_n328,open_n329,open_n330,open_n331,open_n332,inst_doa_i0_004}));
  // address_offset=0;data_offset=5;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_005 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n357,open_n358,addra[13]}),
    .dia({open_n362,open_n363,open_n364,open_n365,open_n366,open_n367,open_n368,dia[5],open_n369}),
    .wea(wea[0]),
    .doa({open_n384,open_n385,open_n386,open_n387,open_n388,open_n389,open_n390,open_n391,inst_doa_i0_005}));
  // address_offset=0;data_offset=6;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_006 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n416,open_n417,addra[13]}),
    .dia({open_n421,open_n422,open_n423,open_n424,open_n425,open_n426,open_n427,dia[6],open_n428}),
    .wea(wea[0]),
    .doa({open_n443,open_n444,open_n445,open_n446,open_n447,open_n448,open_n449,open_n450,inst_doa_i0_006}));
  // address_offset=0;data_offset=7;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_007 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n475,open_n476,addra[13]}),
    .dia({open_n480,open_n481,open_n482,open_n483,open_n484,open_n485,open_n486,dia[7],open_n487}),
    .wea(wea[0]),
    .doa({open_n502,open_n503,open_n504,open_n505,open_n506,open_n507,open_n508,open_n509,inst_doa_i0_007}));
  // address_offset=0;data_offset=8;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_008 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n534,open_n535,addra[13]}),
    .dia({open_n539,open_n540,open_n541,open_n542,open_n543,open_n544,open_n545,dia[8],open_n546}),
    .wea(wea[1]),
    .doa({open_n561,open_n562,open_n563,open_n564,open_n565,open_n566,open_n567,open_n568,inst_doa_i0_008}));
  // address_offset=0;data_offset=9;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_009 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n593,open_n594,addra[13]}),
    .dia({open_n598,open_n599,open_n600,open_n601,open_n602,open_n603,open_n604,dia[9],open_n605}),
    .wea(wea[1]),
    .doa({open_n620,open_n621,open_n622,open_n623,open_n624,open_n625,open_n626,open_n627,inst_doa_i0_009}));
  // address_offset=0;data_offset=10;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_010 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n652,open_n653,addra[13]}),
    .dia({open_n657,open_n658,open_n659,open_n660,open_n661,open_n662,open_n663,dia[10],open_n664}),
    .wea(wea[1]),
    .doa({open_n679,open_n680,open_n681,open_n682,open_n683,open_n684,open_n685,open_n686,inst_doa_i0_010}));
  // address_offset=0;data_offset=11;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_011 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n711,open_n712,addra[13]}),
    .dia({open_n716,open_n717,open_n718,open_n719,open_n720,open_n721,open_n722,dia[11],open_n723}),
    .wea(wea[1]),
    .doa({open_n738,open_n739,open_n740,open_n741,open_n742,open_n743,open_n744,open_n745,inst_doa_i0_011}));
  // address_offset=0;data_offset=12;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_012 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n770,open_n771,addra[13]}),
    .dia({open_n775,open_n776,open_n777,open_n778,open_n779,open_n780,open_n781,dia[12],open_n782}),
    .wea(wea[1]),
    .doa({open_n797,open_n798,open_n799,open_n800,open_n801,open_n802,open_n803,open_n804,inst_doa_i0_012}));
  // address_offset=0;data_offset=13;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_013 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n829,open_n830,addra[13]}),
    .dia({open_n834,open_n835,open_n836,open_n837,open_n838,open_n839,open_n840,dia[13],open_n841}),
    .wea(wea[1]),
    .doa({open_n856,open_n857,open_n858,open_n859,open_n860,open_n861,open_n862,open_n863,inst_doa_i0_013}));
  // address_offset=0;data_offset=14;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_014 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n888,open_n889,addra[13]}),
    .dia({open_n893,open_n894,open_n895,open_n896,open_n897,open_n898,open_n899,dia[14],open_n900}),
    .wea(wea[1]),
    .doa({open_n915,open_n916,open_n917,open_n918,open_n919,open_n920,open_n921,open_n922,inst_doa_i0_014}));
  // address_offset=0;data_offset=15;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_015 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n947,open_n948,addra[13]}),
    .dia({open_n952,open_n953,open_n954,open_n955,open_n956,open_n957,open_n958,dia[15],open_n959}),
    .wea(wea[1]),
    .doa({open_n974,open_n975,open_n976,open_n977,open_n978,open_n979,open_n980,open_n981,inst_doa_i0_015}));
  // address_offset=0;data_offset=16;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_016 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n1006,open_n1007,addra[13]}),
    .dia({open_n1011,open_n1012,open_n1013,open_n1014,open_n1015,open_n1016,open_n1017,dia[16],open_n1018}),
    .wea(wea[2]),
    .doa({open_n1033,open_n1034,open_n1035,open_n1036,open_n1037,open_n1038,open_n1039,open_n1040,inst_doa_i0_016}));
  // address_offset=0;data_offset=17;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_017 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n1065,open_n1066,addra[13]}),
    .dia({open_n1070,open_n1071,open_n1072,open_n1073,open_n1074,open_n1075,open_n1076,dia[17],open_n1077}),
    .wea(wea[2]),
    .doa({open_n1092,open_n1093,open_n1094,open_n1095,open_n1096,open_n1097,open_n1098,open_n1099,inst_doa_i0_017}));
  // address_offset=0;data_offset=18;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_018 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n1124,open_n1125,addra[13]}),
    .dia({open_n1129,open_n1130,open_n1131,open_n1132,open_n1133,open_n1134,open_n1135,dia[18],open_n1136}),
    .wea(wea[2]),
    .doa({open_n1151,open_n1152,open_n1153,open_n1154,open_n1155,open_n1156,open_n1157,open_n1158,inst_doa_i0_018}));
  // address_offset=0;data_offset=19;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_019 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n1183,open_n1184,addra[13]}),
    .dia({open_n1188,open_n1189,open_n1190,open_n1191,open_n1192,open_n1193,open_n1194,dia[19],open_n1195}),
    .wea(wea[2]),
    .doa({open_n1210,open_n1211,open_n1212,open_n1213,open_n1214,open_n1215,open_n1216,open_n1217,inst_doa_i0_019}));
  // address_offset=0;data_offset=20;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_020 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n1242,open_n1243,addra[13]}),
    .dia({open_n1247,open_n1248,open_n1249,open_n1250,open_n1251,open_n1252,open_n1253,dia[20],open_n1254}),
    .wea(wea[2]),
    .doa({open_n1269,open_n1270,open_n1271,open_n1272,open_n1273,open_n1274,open_n1275,open_n1276,inst_doa_i0_020}));
  // address_offset=0;data_offset=21;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_021 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n1301,open_n1302,addra[13]}),
    .dia({open_n1306,open_n1307,open_n1308,open_n1309,open_n1310,open_n1311,open_n1312,dia[21],open_n1313}),
    .wea(wea[2]),
    .doa({open_n1328,open_n1329,open_n1330,open_n1331,open_n1332,open_n1333,open_n1334,open_n1335,inst_doa_i0_021}));
  // address_offset=0;data_offset=22;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_022 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n1360,open_n1361,addra[13]}),
    .dia({open_n1365,open_n1366,open_n1367,open_n1368,open_n1369,open_n1370,open_n1371,dia[22],open_n1372}),
    .wea(wea[2]),
    .doa({open_n1387,open_n1388,open_n1389,open_n1390,open_n1391,open_n1392,open_n1393,open_n1394,inst_doa_i0_022}));
  // address_offset=0;data_offset=23;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_023 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n1419,open_n1420,addra[13]}),
    .dia({open_n1424,open_n1425,open_n1426,open_n1427,open_n1428,open_n1429,open_n1430,dia[23],open_n1431}),
    .wea(wea[2]),
    .doa({open_n1446,open_n1447,open_n1448,open_n1449,open_n1450,open_n1451,open_n1452,open_n1453,inst_doa_i0_023}));
  // address_offset=0;data_offset=24;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_024 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n1478,open_n1479,addra[13]}),
    .dia({open_n1483,open_n1484,open_n1485,open_n1486,open_n1487,open_n1488,open_n1489,dia[24],open_n1490}),
    .wea(wea[3]),
    .doa({open_n1505,open_n1506,open_n1507,open_n1508,open_n1509,open_n1510,open_n1511,open_n1512,inst_doa_i0_024}));
  // address_offset=0;data_offset=25;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_025 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n1537,open_n1538,addra[13]}),
    .dia({open_n1542,open_n1543,open_n1544,open_n1545,open_n1546,open_n1547,open_n1548,dia[25],open_n1549}),
    .wea(wea[3]),
    .doa({open_n1564,open_n1565,open_n1566,open_n1567,open_n1568,open_n1569,open_n1570,open_n1571,inst_doa_i0_025}));
  // address_offset=0;data_offset=26;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_026 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n1596,open_n1597,addra[13]}),
    .dia({open_n1601,open_n1602,open_n1603,open_n1604,open_n1605,open_n1606,open_n1607,dia[26],open_n1608}),
    .wea(wea[3]),
    .doa({open_n1623,open_n1624,open_n1625,open_n1626,open_n1627,open_n1628,open_n1629,open_n1630,inst_doa_i0_026}));
  // address_offset=0;data_offset=27;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_027 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n1655,open_n1656,addra[13]}),
    .dia({open_n1660,open_n1661,open_n1662,open_n1663,open_n1664,open_n1665,open_n1666,dia[27],open_n1667}),
    .wea(wea[3]),
    .doa({open_n1682,open_n1683,open_n1684,open_n1685,open_n1686,open_n1687,open_n1688,open_n1689,inst_doa_i0_027}));
  // address_offset=0;data_offset=28;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_028 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n1714,open_n1715,addra[13]}),
    .dia({open_n1719,open_n1720,open_n1721,open_n1722,open_n1723,open_n1724,open_n1725,dia[28],open_n1726}),
    .wea(wea[3]),
    .doa({open_n1741,open_n1742,open_n1743,open_n1744,open_n1745,open_n1746,open_n1747,open_n1748,inst_doa_i0_028}));
  // address_offset=0;data_offset=29;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_029 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n1773,open_n1774,addra[13]}),
    .dia({open_n1778,open_n1779,open_n1780,open_n1781,open_n1782,open_n1783,open_n1784,dia[29],open_n1785}),
    .wea(wea[3]),
    .doa({open_n1800,open_n1801,open_n1802,open_n1803,open_n1804,open_n1805,open_n1806,open_n1807,inst_doa_i0_029}));
  // address_offset=0;data_offset=30;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_030 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n1832,open_n1833,addra[13]}),
    .dia({open_n1837,open_n1838,open_n1839,open_n1840,open_n1841,open_n1842,open_n1843,dia[30],open_n1844}),
    .wea(wea[3]),
    .doa({open_n1859,open_n1860,open_n1861,open_n1862,open_n1863,open_n1864,open_n1865,open_n1866,inst_doa_i0_030}));
  // address_offset=0;data_offset=31;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("INV"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_000000_031 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n1891,open_n1892,addra[13]}),
    .dia({open_n1896,open_n1897,open_n1898,open_n1899,open_n1900,open_n1901,open_n1902,dia[31],open_n1903}),
    .wea(wea[3]),
    .doa({open_n1918,open_n1919,open_n1920,open_n1921,open_n1922,open_n1923,open_n1924,open_n1925,inst_doa_i0_031}));
  // address_offset=8192;data_offset=0;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_000 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n1950,open_n1951,addra[13]}),
    .dia({open_n1955,open_n1956,open_n1957,open_n1958,open_n1959,open_n1960,open_n1961,dia[0],open_n1962}),
    .wea(wea[0]),
    .doa({open_n1977,open_n1978,open_n1979,open_n1980,open_n1981,open_n1982,open_n1983,open_n1984,inst_doa_i1_000}));
  // address_offset=8192;data_offset=1;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_001 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n2009,open_n2010,addra[13]}),
    .dia({open_n2014,open_n2015,open_n2016,open_n2017,open_n2018,open_n2019,open_n2020,dia[1],open_n2021}),
    .wea(wea[0]),
    .doa({open_n2036,open_n2037,open_n2038,open_n2039,open_n2040,open_n2041,open_n2042,open_n2043,inst_doa_i1_001}));
  // address_offset=8192;data_offset=2;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_002 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n2068,open_n2069,addra[13]}),
    .dia({open_n2073,open_n2074,open_n2075,open_n2076,open_n2077,open_n2078,open_n2079,dia[2],open_n2080}),
    .wea(wea[0]),
    .doa({open_n2095,open_n2096,open_n2097,open_n2098,open_n2099,open_n2100,open_n2101,open_n2102,inst_doa_i1_002}));
  // address_offset=8192;data_offset=3;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_003 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n2127,open_n2128,addra[13]}),
    .dia({open_n2132,open_n2133,open_n2134,open_n2135,open_n2136,open_n2137,open_n2138,dia[3],open_n2139}),
    .wea(wea[0]),
    .doa({open_n2154,open_n2155,open_n2156,open_n2157,open_n2158,open_n2159,open_n2160,open_n2161,inst_doa_i1_003}));
  // address_offset=8192;data_offset=4;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_004 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n2186,open_n2187,addra[13]}),
    .dia({open_n2191,open_n2192,open_n2193,open_n2194,open_n2195,open_n2196,open_n2197,dia[4],open_n2198}),
    .wea(wea[0]),
    .doa({open_n2213,open_n2214,open_n2215,open_n2216,open_n2217,open_n2218,open_n2219,open_n2220,inst_doa_i1_004}));
  // address_offset=8192;data_offset=5;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_005 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n2245,open_n2246,addra[13]}),
    .dia({open_n2250,open_n2251,open_n2252,open_n2253,open_n2254,open_n2255,open_n2256,dia[5],open_n2257}),
    .wea(wea[0]),
    .doa({open_n2272,open_n2273,open_n2274,open_n2275,open_n2276,open_n2277,open_n2278,open_n2279,inst_doa_i1_005}));
  // address_offset=8192;data_offset=6;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_006 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n2304,open_n2305,addra[13]}),
    .dia({open_n2309,open_n2310,open_n2311,open_n2312,open_n2313,open_n2314,open_n2315,dia[6],open_n2316}),
    .wea(wea[0]),
    .doa({open_n2331,open_n2332,open_n2333,open_n2334,open_n2335,open_n2336,open_n2337,open_n2338,inst_doa_i1_006}));
  // address_offset=8192;data_offset=7;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_007 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n2363,open_n2364,addra[13]}),
    .dia({open_n2368,open_n2369,open_n2370,open_n2371,open_n2372,open_n2373,open_n2374,dia[7],open_n2375}),
    .wea(wea[0]),
    .doa({open_n2390,open_n2391,open_n2392,open_n2393,open_n2394,open_n2395,open_n2396,open_n2397,inst_doa_i1_007}));
  // address_offset=8192;data_offset=8;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_008 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n2422,open_n2423,addra[13]}),
    .dia({open_n2427,open_n2428,open_n2429,open_n2430,open_n2431,open_n2432,open_n2433,dia[8],open_n2434}),
    .wea(wea[1]),
    .doa({open_n2449,open_n2450,open_n2451,open_n2452,open_n2453,open_n2454,open_n2455,open_n2456,inst_doa_i1_008}));
  // address_offset=8192;data_offset=9;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_009 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n2481,open_n2482,addra[13]}),
    .dia({open_n2486,open_n2487,open_n2488,open_n2489,open_n2490,open_n2491,open_n2492,dia[9],open_n2493}),
    .wea(wea[1]),
    .doa({open_n2508,open_n2509,open_n2510,open_n2511,open_n2512,open_n2513,open_n2514,open_n2515,inst_doa_i1_009}));
  // address_offset=8192;data_offset=10;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_010 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n2540,open_n2541,addra[13]}),
    .dia({open_n2545,open_n2546,open_n2547,open_n2548,open_n2549,open_n2550,open_n2551,dia[10],open_n2552}),
    .wea(wea[1]),
    .doa({open_n2567,open_n2568,open_n2569,open_n2570,open_n2571,open_n2572,open_n2573,open_n2574,inst_doa_i1_010}));
  // address_offset=8192;data_offset=11;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_011 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n2599,open_n2600,addra[13]}),
    .dia({open_n2604,open_n2605,open_n2606,open_n2607,open_n2608,open_n2609,open_n2610,dia[11],open_n2611}),
    .wea(wea[1]),
    .doa({open_n2626,open_n2627,open_n2628,open_n2629,open_n2630,open_n2631,open_n2632,open_n2633,inst_doa_i1_011}));
  // address_offset=8192;data_offset=12;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_012 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n2658,open_n2659,addra[13]}),
    .dia({open_n2663,open_n2664,open_n2665,open_n2666,open_n2667,open_n2668,open_n2669,dia[12],open_n2670}),
    .wea(wea[1]),
    .doa({open_n2685,open_n2686,open_n2687,open_n2688,open_n2689,open_n2690,open_n2691,open_n2692,inst_doa_i1_012}));
  // address_offset=8192;data_offset=13;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_013 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n2717,open_n2718,addra[13]}),
    .dia({open_n2722,open_n2723,open_n2724,open_n2725,open_n2726,open_n2727,open_n2728,dia[13],open_n2729}),
    .wea(wea[1]),
    .doa({open_n2744,open_n2745,open_n2746,open_n2747,open_n2748,open_n2749,open_n2750,open_n2751,inst_doa_i1_013}));
  // address_offset=8192;data_offset=14;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_014 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n2776,open_n2777,addra[13]}),
    .dia({open_n2781,open_n2782,open_n2783,open_n2784,open_n2785,open_n2786,open_n2787,dia[14],open_n2788}),
    .wea(wea[1]),
    .doa({open_n2803,open_n2804,open_n2805,open_n2806,open_n2807,open_n2808,open_n2809,open_n2810,inst_doa_i1_014}));
  // address_offset=8192;data_offset=15;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_015 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n2835,open_n2836,addra[13]}),
    .dia({open_n2840,open_n2841,open_n2842,open_n2843,open_n2844,open_n2845,open_n2846,dia[15],open_n2847}),
    .wea(wea[1]),
    .doa({open_n2862,open_n2863,open_n2864,open_n2865,open_n2866,open_n2867,open_n2868,open_n2869,inst_doa_i1_015}));
  // address_offset=8192;data_offset=16;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_016 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n2894,open_n2895,addra[13]}),
    .dia({open_n2899,open_n2900,open_n2901,open_n2902,open_n2903,open_n2904,open_n2905,dia[16],open_n2906}),
    .wea(wea[2]),
    .doa({open_n2921,open_n2922,open_n2923,open_n2924,open_n2925,open_n2926,open_n2927,open_n2928,inst_doa_i1_016}));
  // address_offset=8192;data_offset=17;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_017 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n2953,open_n2954,addra[13]}),
    .dia({open_n2958,open_n2959,open_n2960,open_n2961,open_n2962,open_n2963,open_n2964,dia[17],open_n2965}),
    .wea(wea[2]),
    .doa({open_n2980,open_n2981,open_n2982,open_n2983,open_n2984,open_n2985,open_n2986,open_n2987,inst_doa_i1_017}));
  // address_offset=8192;data_offset=18;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_018 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n3012,open_n3013,addra[13]}),
    .dia({open_n3017,open_n3018,open_n3019,open_n3020,open_n3021,open_n3022,open_n3023,dia[18],open_n3024}),
    .wea(wea[2]),
    .doa({open_n3039,open_n3040,open_n3041,open_n3042,open_n3043,open_n3044,open_n3045,open_n3046,inst_doa_i1_018}));
  // address_offset=8192;data_offset=19;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_019 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n3071,open_n3072,addra[13]}),
    .dia({open_n3076,open_n3077,open_n3078,open_n3079,open_n3080,open_n3081,open_n3082,dia[19],open_n3083}),
    .wea(wea[2]),
    .doa({open_n3098,open_n3099,open_n3100,open_n3101,open_n3102,open_n3103,open_n3104,open_n3105,inst_doa_i1_019}));
  // address_offset=8192;data_offset=20;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_020 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n3130,open_n3131,addra[13]}),
    .dia({open_n3135,open_n3136,open_n3137,open_n3138,open_n3139,open_n3140,open_n3141,dia[20],open_n3142}),
    .wea(wea[2]),
    .doa({open_n3157,open_n3158,open_n3159,open_n3160,open_n3161,open_n3162,open_n3163,open_n3164,inst_doa_i1_020}));
  // address_offset=8192;data_offset=21;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_021 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n3189,open_n3190,addra[13]}),
    .dia({open_n3194,open_n3195,open_n3196,open_n3197,open_n3198,open_n3199,open_n3200,dia[21],open_n3201}),
    .wea(wea[2]),
    .doa({open_n3216,open_n3217,open_n3218,open_n3219,open_n3220,open_n3221,open_n3222,open_n3223,inst_doa_i1_021}));
  // address_offset=8192;data_offset=22;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_022 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n3248,open_n3249,addra[13]}),
    .dia({open_n3253,open_n3254,open_n3255,open_n3256,open_n3257,open_n3258,open_n3259,dia[22],open_n3260}),
    .wea(wea[2]),
    .doa({open_n3275,open_n3276,open_n3277,open_n3278,open_n3279,open_n3280,open_n3281,open_n3282,inst_doa_i1_022}));
  // address_offset=8192;data_offset=23;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_023 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n3307,open_n3308,addra[13]}),
    .dia({open_n3312,open_n3313,open_n3314,open_n3315,open_n3316,open_n3317,open_n3318,dia[23],open_n3319}),
    .wea(wea[2]),
    .doa({open_n3334,open_n3335,open_n3336,open_n3337,open_n3338,open_n3339,open_n3340,open_n3341,inst_doa_i1_023}));
  // address_offset=8192;data_offset=24;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_024 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n3366,open_n3367,addra[13]}),
    .dia({open_n3371,open_n3372,open_n3373,open_n3374,open_n3375,open_n3376,open_n3377,dia[24],open_n3378}),
    .wea(wea[3]),
    .doa({open_n3393,open_n3394,open_n3395,open_n3396,open_n3397,open_n3398,open_n3399,open_n3400,inst_doa_i1_024}));
  // address_offset=8192;data_offset=25;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_025 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n3425,open_n3426,addra[13]}),
    .dia({open_n3430,open_n3431,open_n3432,open_n3433,open_n3434,open_n3435,open_n3436,dia[25],open_n3437}),
    .wea(wea[3]),
    .doa({open_n3452,open_n3453,open_n3454,open_n3455,open_n3456,open_n3457,open_n3458,open_n3459,inst_doa_i1_025}));
  // address_offset=8192;data_offset=26;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_026 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n3484,open_n3485,addra[13]}),
    .dia({open_n3489,open_n3490,open_n3491,open_n3492,open_n3493,open_n3494,open_n3495,dia[26],open_n3496}),
    .wea(wea[3]),
    .doa({open_n3511,open_n3512,open_n3513,open_n3514,open_n3515,open_n3516,open_n3517,open_n3518,inst_doa_i1_026}));
  // address_offset=8192;data_offset=27;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_027 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n3543,open_n3544,addra[13]}),
    .dia({open_n3548,open_n3549,open_n3550,open_n3551,open_n3552,open_n3553,open_n3554,dia[27],open_n3555}),
    .wea(wea[3]),
    .doa({open_n3570,open_n3571,open_n3572,open_n3573,open_n3574,open_n3575,open_n3576,open_n3577,inst_doa_i1_027}));
  // address_offset=8192;data_offset=28;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_028 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n3602,open_n3603,addra[13]}),
    .dia({open_n3607,open_n3608,open_n3609,open_n3610,open_n3611,open_n3612,open_n3613,dia[28],open_n3614}),
    .wea(wea[3]),
    .doa({open_n3629,open_n3630,open_n3631,open_n3632,open_n3633,open_n3634,open_n3635,open_n3636,inst_doa_i1_028}));
  // address_offset=8192;data_offset=29;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_029 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n3661,open_n3662,addra[13]}),
    .dia({open_n3666,open_n3667,open_n3668,open_n3669,open_n3670,open_n3671,open_n3672,dia[29],open_n3673}),
    .wea(wea[3]),
    .doa({open_n3688,open_n3689,open_n3690,open_n3691,open_n3692,open_n3693,open_n3694,open_n3695,inst_doa_i1_029}));
  // address_offset=8192;data_offset=30;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_030 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n3720,open_n3721,addra[13]}),
    .dia({open_n3725,open_n3726,open_n3727,open_n3728,open_n3729,open_n3730,open_n3731,dia[30],open_n3732}),
    .wea(wea[3]),
    .doa({open_n3747,open_n3748,open_n3749,open_n3750,open_n3751,open_n3752,open_n3753,open_n3754,inst_doa_i1_030}));
  // address_offset=8192;data_offset=31;depth=8192;width=1;num_section=1;width_per_section=1;section_size=32;working_depth=8192;working_width=1;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM #(
    .CEBMUX("0"),
    .CLKBMUX("0"),
    .CSA0("SIG"),
    .CSA1("1"),
    .CSA2("1"),
    .CSB0("1"),
    .CSB1("1"),
    .CSB2("1"),
    .DATA_WIDTH_A("1"),
    .DATA_WIDTH_B("1"),
    .MODE("SP8K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RESETMODE("SYNC"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_16384x32_sub_008192_031 (
    .addra(addra[12:0]),
    .cea(cea),
    .clka(clka),
    .csa({open_n3779,open_n3780,addra[13]}),
    .dia({open_n3784,open_n3785,open_n3786,open_n3787,open_n3788,open_n3789,open_n3790,dia[31],open_n3791}),
    .wea(wea[3]),
    .doa({open_n3806,open_n3807,open_n3808,open_n3809,open_n3810,open_n3811,open_n3812,open_n3813,inst_doa_i1_031}));
  AL_MUX \inst_doa_mux_b0/al_mux_b0_0_0  (
    .i0(inst_doa_i0_000),
    .i1(inst_doa_i1_000),
    .sel(addra_piped),
    .o(doa[0]));
  AL_MUX \inst_doa_mux_b1/al_mux_b0_0_0  (
    .i0(inst_doa_i0_001),
    .i1(inst_doa_i1_001),
    .sel(addra_piped),
    .o(doa[1]));
  AL_MUX \inst_doa_mux_b10/al_mux_b0_0_0  (
    .i0(inst_doa_i0_010),
    .i1(inst_doa_i1_010),
    .sel(addra_piped),
    .o(doa[10]));
  AL_MUX \inst_doa_mux_b11/al_mux_b0_0_0  (
    .i0(inst_doa_i0_011),
    .i1(inst_doa_i1_011),
    .sel(addra_piped),
    .o(doa[11]));
  AL_MUX \inst_doa_mux_b12/al_mux_b0_0_0  (
    .i0(inst_doa_i0_012),
    .i1(inst_doa_i1_012),
    .sel(addra_piped),
    .o(doa[12]));
  AL_MUX \inst_doa_mux_b13/al_mux_b0_0_0  (
    .i0(inst_doa_i0_013),
    .i1(inst_doa_i1_013),
    .sel(addra_piped),
    .o(doa[13]));
  AL_MUX \inst_doa_mux_b14/al_mux_b0_0_0  (
    .i0(inst_doa_i0_014),
    .i1(inst_doa_i1_014),
    .sel(addra_piped),
    .o(doa[14]));
  AL_MUX \inst_doa_mux_b15/al_mux_b0_0_0  (
    .i0(inst_doa_i0_015),
    .i1(inst_doa_i1_015),
    .sel(addra_piped),
    .o(doa[15]));
  AL_MUX \inst_doa_mux_b16/al_mux_b0_0_0  (
    .i0(inst_doa_i0_016),
    .i1(inst_doa_i1_016),
    .sel(addra_piped),
    .o(doa[16]));
  AL_MUX \inst_doa_mux_b17/al_mux_b0_0_0  (
    .i0(inst_doa_i0_017),
    .i1(inst_doa_i1_017),
    .sel(addra_piped),
    .o(doa[17]));
  AL_MUX \inst_doa_mux_b18/al_mux_b0_0_0  (
    .i0(inst_doa_i0_018),
    .i1(inst_doa_i1_018),
    .sel(addra_piped),
    .o(doa[18]));
  AL_MUX \inst_doa_mux_b19/al_mux_b0_0_0  (
    .i0(inst_doa_i0_019),
    .i1(inst_doa_i1_019),
    .sel(addra_piped),
    .o(doa[19]));
  AL_MUX \inst_doa_mux_b2/al_mux_b0_0_0  (
    .i0(inst_doa_i0_002),
    .i1(inst_doa_i1_002),
    .sel(addra_piped),
    .o(doa[2]));
  AL_MUX \inst_doa_mux_b20/al_mux_b0_0_0  (
    .i0(inst_doa_i0_020),
    .i1(inst_doa_i1_020),
    .sel(addra_piped),
    .o(doa[20]));
  AL_MUX \inst_doa_mux_b21/al_mux_b0_0_0  (
    .i0(inst_doa_i0_021),
    .i1(inst_doa_i1_021),
    .sel(addra_piped),
    .o(doa[21]));
  AL_MUX \inst_doa_mux_b22/al_mux_b0_0_0  (
    .i0(inst_doa_i0_022),
    .i1(inst_doa_i1_022),
    .sel(addra_piped),
    .o(doa[22]));
  AL_MUX \inst_doa_mux_b23/al_mux_b0_0_0  (
    .i0(inst_doa_i0_023),
    .i1(inst_doa_i1_023),
    .sel(addra_piped),
    .o(doa[23]));
  AL_MUX \inst_doa_mux_b24/al_mux_b0_0_0  (
    .i0(inst_doa_i0_024),
    .i1(inst_doa_i1_024),
    .sel(addra_piped),
    .o(doa[24]));
  AL_MUX \inst_doa_mux_b25/al_mux_b0_0_0  (
    .i0(inst_doa_i0_025),
    .i1(inst_doa_i1_025),
    .sel(addra_piped),
    .o(doa[25]));
  AL_MUX \inst_doa_mux_b26/al_mux_b0_0_0  (
    .i0(inst_doa_i0_026),
    .i1(inst_doa_i1_026),
    .sel(addra_piped),
    .o(doa[26]));
  AL_MUX \inst_doa_mux_b27/al_mux_b0_0_0  (
    .i0(inst_doa_i0_027),
    .i1(inst_doa_i1_027),
    .sel(addra_piped),
    .o(doa[27]));
  AL_MUX \inst_doa_mux_b28/al_mux_b0_0_0  (
    .i0(inst_doa_i0_028),
    .i1(inst_doa_i1_028),
    .sel(addra_piped),
    .o(doa[28]));
  AL_MUX \inst_doa_mux_b29/al_mux_b0_0_0  (
    .i0(inst_doa_i0_029),
    .i1(inst_doa_i1_029),
    .sel(addra_piped),
    .o(doa[29]));
  AL_MUX \inst_doa_mux_b3/al_mux_b0_0_0  (
    .i0(inst_doa_i0_003),
    .i1(inst_doa_i1_003),
    .sel(addra_piped),
    .o(doa[3]));
  AL_MUX \inst_doa_mux_b30/al_mux_b0_0_0  (
    .i0(inst_doa_i0_030),
    .i1(inst_doa_i1_030),
    .sel(addra_piped),
    .o(doa[30]));
  AL_MUX \inst_doa_mux_b31/al_mux_b0_0_0  (
    .i0(inst_doa_i0_031),
    .i1(inst_doa_i1_031),
    .sel(addra_piped),
    .o(doa[31]));
  AL_MUX \inst_doa_mux_b4/al_mux_b0_0_0  (
    .i0(inst_doa_i0_004),
    .i1(inst_doa_i1_004),
    .sel(addra_piped),
    .o(doa[4]));
  AL_MUX \inst_doa_mux_b5/al_mux_b0_0_0  (
    .i0(inst_doa_i0_005),
    .i1(inst_doa_i1_005),
    .sel(addra_piped),
    .o(doa[5]));
  AL_MUX \inst_doa_mux_b6/al_mux_b0_0_0  (
    .i0(inst_doa_i0_006),
    .i1(inst_doa_i1_006),
    .sel(addra_piped),
    .o(doa[6]));
  AL_MUX \inst_doa_mux_b7/al_mux_b0_0_0  (
    .i0(inst_doa_i0_007),
    .i1(inst_doa_i1_007),
    .sel(addra_piped),
    .o(doa[7]));
  AL_MUX \inst_doa_mux_b8/al_mux_b0_0_0  (
    .i0(inst_doa_i0_008),
    .i1(inst_doa_i1_008),
    .sel(addra_piped),
    .o(doa[8]));
  AL_MUX \inst_doa_mux_b9/al_mux_b0_0_0  (
    .i0(inst_doa_i0_009),
    .i1(inst_doa_i1_009),
    .sel(addra_piped),
    .o(doa[9]));

endmodule 

module AL_DFF_X
  (
  ar,
  as,
  clk,
  d,
  en,
  sr,
  ss,
  q
  );

  input ar;
  input as;
  input clk;
  input d;
  input en;
  input sr;
  input ss;
  output q;

  wire enout;
  wire srout;
  wire ssout;

  AL_MUX u_en (
    .i0(q),
    .i1(d),
    .sel(en),
    .o(enout));
  AL_MUX u_reset (
    .i0(ssout),
    .i1(1'b0),
    .sel(sr),
    .o(srout));
  AL_DFF u_seq (
    .clk(clk),
    .d(srout),
    .reset(ar),
    .set(as),
    .q(q));
  AL_MUX u_set (
    .i0(enout),
    .i1(1'b1),
    .sel(ss),
    .o(ssout));

endmodule 

module AL_MUX
  (
  input i0,
  input i1,
  input sel,
  output o
  );

  wire not_sel, sel_i0, sel_i1;
  not u0 (not_sel, sel);
  and u1 (sel_i1, sel, i1);
  and u2 (sel_i0, not_sel, i0);
  or u3 (o, sel_i1, sel_i0);

endmodule

module AL_DFF
  (
  input reset,
  input set,
  input clk,
  input d,
  output reg q
  );

  parameter INI = 1'b0;

  // synthesis translate_off
  tri0 gsrn = glbl.gsrn;

  always @(gsrn)
  begin
    if(!gsrn)
      assign q = INI;
    else
      deassign q;
  end
  // synthesis translate_on

  always @(posedge reset or posedge set or posedge clk)
  begin
    if (reset)
      q <= 1'b0;
    else if (set)
      q <= 1'b1;
    else
      q <= d;
  end

endmodule

