// Verilog netlist created by TD v5.0.28716
// Wed Apr  7 08:18:47 2021

`timescale 1ns / 1ps
module bram_itcm  // bram_itcm.v(14)
  (
  addra,
  cea,
  clka,
  dia,
  wea,
  doa
  );

  input [12:0] addra;  // bram_itcm.v(29)
  input cea;  // bram_itcm.v(31)
  input clka;  // bram_itcm.v(32)
  input [63:0] dia;  // bram_itcm.v(28)
  input [7:0] wea;  // bram_itcm.v(30)
  output [63:0] doa;  // bram_itcm.v(26)

  parameter ADDR_WIDTH_A = 13;
  parameter ADDR_WIDTH_B = 13;
  parameter DATA_DEPTH_A = 8192;
  parameter DATA_DEPTH_B = 8192;
  parameter DATA_WIDTH_A = 64;
  parameter DATA_WIDTH_B = 64;
  parameter REGMODE_A = "NOREG";
  parameter WRITEMODE_A = "NORMAL";
  wire [0:0] addra_piped;
  wire \addra[12]_neg ;
  wire \and_addra[12]_cea_o ;
  wire \and_addra[12]_neg_al_o ;
  wire inst_doa_i0_000;
  wire inst_doa_i0_001;
  wire inst_doa_i0_002;
  wire inst_doa_i0_003;
  wire inst_doa_i0_004;
  wire inst_doa_i0_005;
  wire inst_doa_i0_006;
  wire inst_doa_i0_007;
  wire inst_doa_i0_008;
  wire inst_doa_i0_009;
  wire inst_doa_i0_010;
  wire inst_doa_i0_011;
  wire inst_doa_i0_012;
  wire inst_doa_i0_013;
  wire inst_doa_i0_014;
  wire inst_doa_i0_015;
  wire inst_doa_i0_016;
  wire inst_doa_i0_017;
  wire inst_doa_i0_018;
  wire inst_doa_i0_019;
  wire inst_doa_i0_020;
  wire inst_doa_i0_021;
  wire inst_doa_i0_022;
  wire inst_doa_i0_023;
  wire inst_doa_i0_024;
  wire inst_doa_i0_025;
  wire inst_doa_i0_026;
  wire inst_doa_i0_027;
  wire inst_doa_i0_028;
  wire inst_doa_i0_029;
  wire inst_doa_i0_030;
  wire inst_doa_i0_031;
  wire inst_doa_i0_032;
  wire inst_doa_i0_033;
  wire inst_doa_i0_034;
  wire inst_doa_i0_035;
  wire inst_doa_i0_036;
  wire inst_doa_i0_037;
  wire inst_doa_i0_038;
  wire inst_doa_i0_039;
  wire inst_doa_i0_040;
  wire inst_doa_i0_041;
  wire inst_doa_i0_042;
  wire inst_doa_i0_043;
  wire inst_doa_i0_044;
  wire inst_doa_i0_045;
  wire inst_doa_i0_046;
  wire inst_doa_i0_047;
  wire inst_doa_i0_048;
  wire inst_doa_i0_049;
  wire inst_doa_i0_050;
  wire inst_doa_i0_051;
  wire inst_doa_i0_052;
  wire inst_doa_i0_053;
  wire inst_doa_i0_054;
  wire inst_doa_i0_055;
  wire inst_doa_i0_056;
  wire inst_doa_i0_057;
  wire inst_doa_i0_058;
  wire inst_doa_i0_059;
  wire inst_doa_i0_060;
  wire inst_doa_i0_061;
  wire inst_doa_i0_062;
  wire inst_doa_i0_063;
  wire inst_doa_i1_000;
  wire inst_doa_i1_001;
  wire inst_doa_i1_002;
  wire inst_doa_i1_003;
  wire inst_doa_i1_004;
  wire inst_doa_i1_005;
  wire inst_doa_i1_006;
  wire inst_doa_i1_007;
  wire inst_doa_i1_008;
  wire inst_doa_i1_009;
  wire inst_doa_i1_010;
  wire inst_doa_i1_011;
  wire inst_doa_i1_012;
  wire inst_doa_i1_013;
  wire inst_doa_i1_014;
  wire inst_doa_i1_015;
  wire inst_doa_i1_016;
  wire inst_doa_i1_017;
  wire inst_doa_i1_018;
  wire inst_doa_i1_019;
  wire inst_doa_i1_020;
  wire inst_doa_i1_021;
  wire inst_doa_i1_022;
  wire inst_doa_i1_023;
  wire inst_doa_i1_024;
  wire inst_doa_i1_025;
  wire inst_doa_i1_026;
  wire inst_doa_i1_027;
  wire inst_doa_i1_028;
  wire inst_doa_i1_029;
  wire inst_doa_i1_030;
  wire inst_doa_i1_031;
  wire inst_doa_i1_032;
  wire inst_doa_i1_033;
  wire inst_doa_i1_034;
  wire inst_doa_i1_035;
  wire inst_doa_i1_036;
  wire inst_doa_i1_037;
  wire inst_doa_i1_038;
  wire inst_doa_i1_039;
  wire inst_doa_i1_040;
  wire inst_doa_i1_041;
  wire inst_doa_i1_042;
  wire inst_doa_i1_043;
  wire inst_doa_i1_044;
  wire inst_doa_i1_045;
  wire inst_doa_i1_046;
  wire inst_doa_i1_047;
  wire inst_doa_i1_048;
  wire inst_doa_i1_049;
  wire inst_doa_i1_050;
  wire inst_doa_i1_051;
  wire inst_doa_i1_052;
  wire inst_doa_i1_053;
  wire inst_doa_i1_054;
  wire inst_doa_i1_055;
  wire inst_doa_i1_056;
  wire inst_doa_i1_057;
  wire inst_doa_i1_058;
  wire inst_doa_i1_059;
  wire inst_doa_i1_060;
  wire inst_doa_i1_061;
  wire inst_doa_i1_062;
  wire inst_doa_i1_063;

  not \addra[12]_inv  (\addra[12]_neg , addra[12]);
  AL_DFF_X addra_pipe (
    .ar(1'b0),
    .as(1'b0),
    .clk(clka),
    .d(addra[12]),
    .en(cea),
    .sr(1'b0),
    .ss(1'b0),
    .q(addra_piped));
  and \and_addra[12]_cea  (\and_addra[12]_cea_o , addra[12], cea);
  and \and_addra[12]_neg_al  (\and_addra[12]_neg_al_o , \addra[12]_neg , cea);
  EG_PHY_CONFIG #(
    .DONE_PERSISTN("ENABLE"),
    .INIT_PERSISTN("ENABLE"),
    .JTAG_PERSISTN("DISABLE"),
    .PROGRAMN_PERSISTN("DISABLE"))
    config_inst ();
  // address_offset=0;data_offset=0;depth=4096;width=8;num_section=1;width_per_section=8;section_size=64;working_depth=4096;working_width=8;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM32K #(
    .CLKBMUX("0"),
    .CSBMUX("0"),
    .DATA_WIDTH_A("8"),
    .DATA_WIDTH_B("8"),
    .MODE("SP16K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .SRMODE("SYNC"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_8192x64_sub_000000_000 (
    .addra(addra[11:1]),
    .addrb(11'b00000000000),
    .bytea(addra[0]),
    .byteb(1'b0),
    .clka(clka),
    .csa(\and_addra[12]_neg_al_o ),
    .dia({open_n51,open_n52,open_n53,open_n54,open_n55,open_n56,open_n57,open_n58,dia[7:0]}),
    .wea(wea[0]),
    .doa({open_n80,open_n81,open_n82,open_n83,open_n84,open_n85,open_n86,open_n87,inst_doa_i0_007,inst_doa_i0_006,inst_doa_i0_005,inst_doa_i0_004,inst_doa_i0_003,inst_doa_i0_002,inst_doa_i0_001,inst_doa_i0_000}));
  // address_offset=0;data_offset=8;depth=4096;width=8;num_section=1;width_per_section=8;section_size=64;working_depth=4096;working_width=8;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM32K #(
    .CLKBMUX("0"),
    .CSBMUX("0"),
    .DATA_WIDTH_A("8"),
    .DATA_WIDTH_B("8"),
    .MODE("SP16K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .SRMODE("SYNC"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_8192x64_sub_000000_008 (
    .addra(addra[11:1]),
    .addrb(11'b00000000000),
    .bytea(addra[0]),
    .byteb(1'b0),
    .clka(clka),
    .csa(\and_addra[12]_neg_al_o ),
    .dia({open_n108,open_n109,open_n110,open_n111,open_n112,open_n113,open_n114,open_n115,dia[15:8]}),
    .wea(wea[1]),
    .doa({open_n137,open_n138,open_n139,open_n140,open_n141,open_n142,open_n143,open_n144,inst_doa_i0_015,inst_doa_i0_014,inst_doa_i0_013,inst_doa_i0_012,inst_doa_i0_011,inst_doa_i0_010,inst_doa_i0_009,inst_doa_i0_008}));
  // address_offset=0;data_offset=16;depth=4096;width=8;num_section=1;width_per_section=8;section_size=64;working_depth=4096;working_width=8;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM32K #(
    .CLKBMUX("0"),
    .CSBMUX("0"),
    .DATA_WIDTH_A("8"),
    .DATA_WIDTH_B("8"),
    .MODE("SP16K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .SRMODE("SYNC"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_8192x64_sub_000000_016 (
    .addra(addra[11:1]),
    .addrb(11'b00000000000),
    .bytea(addra[0]),
    .byteb(1'b0),
    .clka(clka),
    .csa(\and_addra[12]_neg_al_o ),
    .dia({open_n165,open_n166,open_n167,open_n168,open_n169,open_n170,open_n171,open_n172,dia[23:16]}),
    .wea(wea[2]),
    .doa({open_n194,open_n195,open_n196,open_n197,open_n198,open_n199,open_n200,open_n201,inst_doa_i0_023,inst_doa_i0_022,inst_doa_i0_021,inst_doa_i0_020,inst_doa_i0_019,inst_doa_i0_018,inst_doa_i0_017,inst_doa_i0_016}));
  // address_offset=0;data_offset=24;depth=4096;width=8;num_section=1;width_per_section=8;section_size=64;working_depth=4096;working_width=8;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM32K #(
    .CLKBMUX("0"),
    .CSBMUX("0"),
    .DATA_WIDTH_A("8"),
    .DATA_WIDTH_B("8"),
    .MODE("SP16K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .SRMODE("SYNC"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_8192x64_sub_000000_024 (
    .addra(addra[11:1]),
    .addrb(11'b00000000000),
    .bytea(addra[0]),
    .byteb(1'b0),
    .clka(clka),
    .csa(\and_addra[12]_neg_al_o ),
    .dia({open_n222,open_n223,open_n224,open_n225,open_n226,open_n227,open_n228,open_n229,dia[31:24]}),
    .wea(wea[3]),
    .doa({open_n251,open_n252,open_n253,open_n254,open_n255,open_n256,open_n257,open_n258,inst_doa_i0_031,inst_doa_i0_030,inst_doa_i0_029,inst_doa_i0_028,inst_doa_i0_027,inst_doa_i0_026,inst_doa_i0_025,inst_doa_i0_024}));
  // address_offset=0;data_offset=32;depth=4096;width=8;num_section=1;width_per_section=8;section_size=64;working_depth=4096;working_width=8;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM32K #(
    .CLKBMUX("0"),
    .CSBMUX("0"),
    .DATA_WIDTH_A("8"),
    .DATA_WIDTH_B("8"),
    .MODE("SP16K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .SRMODE("SYNC"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_8192x64_sub_000000_032 (
    .addra(addra[11:1]),
    .addrb(11'b00000000000),
    .bytea(addra[0]),
    .byteb(1'b0),
    .clka(clka),
    .csa(\and_addra[12]_neg_al_o ),
    .dia({open_n279,open_n280,open_n281,open_n282,open_n283,open_n284,open_n285,open_n286,dia[39:32]}),
    .wea(wea[4]),
    .doa({open_n308,open_n309,open_n310,open_n311,open_n312,open_n313,open_n314,open_n315,inst_doa_i0_039,inst_doa_i0_038,inst_doa_i0_037,inst_doa_i0_036,inst_doa_i0_035,inst_doa_i0_034,inst_doa_i0_033,inst_doa_i0_032}));
  // address_offset=0;data_offset=40;depth=4096;width=8;num_section=1;width_per_section=8;section_size=64;working_depth=4096;working_width=8;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM32K #(
    .CLKBMUX("0"),
    .CSBMUX("0"),
    .DATA_WIDTH_A("8"),
    .DATA_WIDTH_B("8"),
    .MODE("SP16K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .SRMODE("SYNC"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_8192x64_sub_000000_040 (
    .addra(addra[11:1]),
    .addrb(11'b00000000000),
    .bytea(addra[0]),
    .byteb(1'b0),
    .clka(clka),
    .csa(\and_addra[12]_neg_al_o ),
    .dia({open_n336,open_n337,open_n338,open_n339,open_n340,open_n341,open_n342,open_n343,dia[47:40]}),
    .wea(wea[5]),
    .doa({open_n365,open_n366,open_n367,open_n368,open_n369,open_n370,open_n371,open_n372,inst_doa_i0_047,inst_doa_i0_046,inst_doa_i0_045,inst_doa_i0_044,inst_doa_i0_043,inst_doa_i0_042,inst_doa_i0_041,inst_doa_i0_040}));
  // address_offset=0;data_offset=48;depth=4096;width=8;num_section=1;width_per_section=8;section_size=64;working_depth=4096;working_width=8;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM32K #(
    .CLKBMUX("0"),
    .CSBMUX("0"),
    .DATA_WIDTH_A("8"),
    .DATA_WIDTH_B("8"),
    .MODE("SP16K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .SRMODE("SYNC"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_8192x64_sub_000000_048 (
    .addra(addra[11:1]),
    .addrb(11'b00000000000),
    .bytea(addra[0]),
    .byteb(1'b0),
    .clka(clka),
    .csa(\and_addra[12]_neg_al_o ),
    .dia({open_n393,open_n394,open_n395,open_n396,open_n397,open_n398,open_n399,open_n400,dia[55:48]}),
    .wea(wea[6]),
    .doa({open_n422,open_n423,open_n424,open_n425,open_n426,open_n427,open_n428,open_n429,inst_doa_i0_055,inst_doa_i0_054,inst_doa_i0_053,inst_doa_i0_052,inst_doa_i0_051,inst_doa_i0_050,inst_doa_i0_049,inst_doa_i0_048}));
  // address_offset=0;data_offset=56;depth=4096;width=8;num_section=1;width_per_section=8;section_size=64;working_depth=4096;working_width=8;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM32K #(
    .CLKBMUX("0"),
    .CSBMUX("0"),
    .DATA_WIDTH_A("8"),
    .DATA_WIDTH_B("8"),
    .MODE("SP16K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .SRMODE("SYNC"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_8192x64_sub_000000_056 (
    .addra(addra[11:1]),
    .addrb(11'b00000000000),
    .bytea(addra[0]),
    .byteb(1'b0),
    .clka(clka),
    .csa(\and_addra[12]_neg_al_o ),
    .dia({open_n450,open_n451,open_n452,open_n453,open_n454,open_n455,open_n456,open_n457,dia[63:56]}),
    .wea(wea[7]),
    .doa({open_n479,open_n480,open_n481,open_n482,open_n483,open_n484,open_n485,open_n486,inst_doa_i0_063,inst_doa_i0_062,inst_doa_i0_061,inst_doa_i0_060,inst_doa_i0_059,inst_doa_i0_058,inst_doa_i0_057,inst_doa_i0_056}));
  // address_offset=4096;data_offset=0;depth=4096;width=8;num_section=1;width_per_section=8;section_size=64;working_depth=4096;working_width=8;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM32K #(
    .CLKBMUX("0"),
    .CSBMUX("0"),
    .DATA_WIDTH_A("8"),
    .DATA_WIDTH_B("8"),
    .MODE("SP16K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .SRMODE("SYNC"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_8192x64_sub_004096_000 (
    .addra(addra[11:1]),
    .addrb(11'b00000000000),
    .bytea(addra[0]),
    .byteb(1'b0),
    .clka(clka),
    .csa(\and_addra[12]_cea_o ),
    .dia({open_n507,open_n508,open_n509,open_n510,open_n511,open_n512,open_n513,open_n514,dia[7:0]}),
    .wea(wea[0]),
    .doa({open_n536,open_n537,open_n538,open_n539,open_n540,open_n541,open_n542,open_n543,inst_doa_i1_007,inst_doa_i1_006,inst_doa_i1_005,inst_doa_i1_004,inst_doa_i1_003,inst_doa_i1_002,inst_doa_i1_001,inst_doa_i1_000}));
  // address_offset=4096;data_offset=8;depth=4096;width=8;num_section=1;width_per_section=8;section_size=64;working_depth=4096;working_width=8;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM32K #(
    .CLKBMUX("0"),
    .CSBMUX("0"),
    .DATA_WIDTH_A("8"),
    .DATA_WIDTH_B("8"),
    .MODE("SP16K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .SRMODE("SYNC"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_8192x64_sub_004096_008 (
    .addra(addra[11:1]),
    .addrb(11'b00000000000),
    .bytea(addra[0]),
    .byteb(1'b0),
    .clka(clka),
    .csa(\and_addra[12]_cea_o ),
    .dia({open_n564,open_n565,open_n566,open_n567,open_n568,open_n569,open_n570,open_n571,dia[15:8]}),
    .wea(wea[1]),
    .doa({open_n593,open_n594,open_n595,open_n596,open_n597,open_n598,open_n599,open_n600,inst_doa_i1_015,inst_doa_i1_014,inst_doa_i1_013,inst_doa_i1_012,inst_doa_i1_011,inst_doa_i1_010,inst_doa_i1_009,inst_doa_i1_008}));
  // address_offset=4096;data_offset=16;depth=4096;width=8;num_section=1;width_per_section=8;section_size=64;working_depth=4096;working_width=8;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM32K #(
    .CLKBMUX("0"),
    .CSBMUX("0"),
    .DATA_WIDTH_A("8"),
    .DATA_WIDTH_B("8"),
    .MODE("SP16K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .SRMODE("SYNC"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_8192x64_sub_004096_016 (
    .addra(addra[11:1]),
    .addrb(11'b00000000000),
    .bytea(addra[0]),
    .byteb(1'b0),
    .clka(clka),
    .csa(\and_addra[12]_cea_o ),
    .dia({open_n621,open_n622,open_n623,open_n624,open_n625,open_n626,open_n627,open_n628,dia[23:16]}),
    .wea(wea[2]),
    .doa({open_n650,open_n651,open_n652,open_n653,open_n654,open_n655,open_n656,open_n657,inst_doa_i1_023,inst_doa_i1_022,inst_doa_i1_021,inst_doa_i1_020,inst_doa_i1_019,inst_doa_i1_018,inst_doa_i1_017,inst_doa_i1_016}));
  // address_offset=4096;data_offset=24;depth=4096;width=8;num_section=1;width_per_section=8;section_size=64;working_depth=4096;working_width=8;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM32K #(
    .CLKBMUX("0"),
    .CSBMUX("0"),
    .DATA_WIDTH_A("8"),
    .DATA_WIDTH_B("8"),
    .MODE("SP16K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .SRMODE("SYNC"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_8192x64_sub_004096_024 (
    .addra(addra[11:1]),
    .addrb(11'b00000000000),
    .bytea(addra[0]),
    .byteb(1'b0),
    .clka(clka),
    .csa(\and_addra[12]_cea_o ),
    .dia({open_n678,open_n679,open_n680,open_n681,open_n682,open_n683,open_n684,open_n685,dia[31:24]}),
    .wea(wea[3]),
    .doa({open_n707,open_n708,open_n709,open_n710,open_n711,open_n712,open_n713,open_n714,inst_doa_i1_031,inst_doa_i1_030,inst_doa_i1_029,inst_doa_i1_028,inst_doa_i1_027,inst_doa_i1_026,inst_doa_i1_025,inst_doa_i1_024}));
  // address_offset=4096;data_offset=32;depth=4096;width=8;num_section=1;width_per_section=8;section_size=64;working_depth=4096;working_width=8;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM32K #(
    .CLKBMUX("0"),
    .CSBMUX("0"),
    .DATA_WIDTH_A("8"),
    .DATA_WIDTH_B("8"),
    .MODE("SP16K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .SRMODE("SYNC"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_8192x64_sub_004096_032 (
    .addra(addra[11:1]),
    .addrb(11'b00000000000),
    .bytea(addra[0]),
    .byteb(1'b0),
    .clka(clka),
    .csa(\and_addra[12]_cea_o ),
    .dia({open_n735,open_n736,open_n737,open_n738,open_n739,open_n740,open_n741,open_n742,dia[39:32]}),
    .wea(wea[4]),
    .doa({open_n764,open_n765,open_n766,open_n767,open_n768,open_n769,open_n770,open_n771,inst_doa_i1_039,inst_doa_i1_038,inst_doa_i1_037,inst_doa_i1_036,inst_doa_i1_035,inst_doa_i1_034,inst_doa_i1_033,inst_doa_i1_032}));
  // address_offset=4096;data_offset=40;depth=4096;width=8;num_section=1;width_per_section=8;section_size=64;working_depth=4096;working_width=8;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM32K #(
    .CLKBMUX("0"),
    .CSBMUX("0"),
    .DATA_WIDTH_A("8"),
    .DATA_WIDTH_B("8"),
    .MODE("SP16K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .SRMODE("SYNC"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_8192x64_sub_004096_040 (
    .addra(addra[11:1]),
    .addrb(11'b00000000000),
    .bytea(addra[0]),
    .byteb(1'b0),
    .clka(clka),
    .csa(\and_addra[12]_cea_o ),
    .dia({open_n792,open_n793,open_n794,open_n795,open_n796,open_n797,open_n798,open_n799,dia[47:40]}),
    .wea(wea[5]),
    .doa({open_n821,open_n822,open_n823,open_n824,open_n825,open_n826,open_n827,open_n828,inst_doa_i1_047,inst_doa_i1_046,inst_doa_i1_045,inst_doa_i1_044,inst_doa_i1_043,inst_doa_i1_042,inst_doa_i1_041,inst_doa_i1_040}));
  // address_offset=4096;data_offset=48;depth=4096;width=8;num_section=1;width_per_section=8;section_size=64;working_depth=4096;working_width=8;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM32K #(
    .CLKBMUX("0"),
    .CSBMUX("0"),
    .DATA_WIDTH_A("8"),
    .DATA_WIDTH_B("8"),
    .MODE("SP16K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .SRMODE("SYNC"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_8192x64_sub_004096_048 (
    .addra(addra[11:1]),
    .addrb(11'b00000000000),
    .bytea(addra[0]),
    .byteb(1'b0),
    .clka(clka),
    .csa(\and_addra[12]_cea_o ),
    .dia({open_n849,open_n850,open_n851,open_n852,open_n853,open_n854,open_n855,open_n856,dia[55:48]}),
    .wea(wea[6]),
    .doa({open_n878,open_n879,open_n880,open_n881,open_n882,open_n883,open_n884,open_n885,inst_doa_i1_055,inst_doa_i1_054,inst_doa_i1_053,inst_doa_i1_052,inst_doa_i1_051,inst_doa_i1_050,inst_doa_i1_049,inst_doa_i1_048}));
  // address_offset=4096;data_offset=56;depth=4096;width=8;num_section=1;width_per_section=8;section_size=64;working_depth=4096;working_width=8;mode_ecc=0;address_step=1;bytes_in_per_section=1;
  EG_PHY_BRAM32K #(
    .CLKBMUX("0"),
    .CSBMUX("0"),
    .DATA_WIDTH_A("8"),
    .DATA_WIDTH_B("8"),
    .MODE("SP16K"),
    .OCEAMUX("0"),
    .OCEBMUX("0"),
    .REGMODE_A("NOREG"),
    .REGMODE_B("NOREG"),
    .RSTAMUX("0"),
    .RSTBMUX("0"),
    .SRMODE("SYNC"),
    .WEBMUX("0"),
    .WRITEMODE_A("NORMAL"),
    .WRITEMODE_B("NORMAL"))
    inst_8192x64_sub_004096_056 (
    .addra(addra[11:1]),
    .addrb(11'b00000000000),
    .bytea(addra[0]),
    .byteb(1'b0),
    .clka(clka),
    .csa(\and_addra[12]_cea_o ),
    .dia({open_n906,open_n907,open_n908,open_n909,open_n910,open_n911,open_n912,open_n913,dia[63:56]}),
    .wea(wea[7]),
    .doa({open_n935,open_n936,open_n937,open_n938,open_n939,open_n940,open_n941,open_n942,inst_doa_i1_063,inst_doa_i1_062,inst_doa_i1_061,inst_doa_i1_060,inst_doa_i1_059,inst_doa_i1_058,inst_doa_i1_057,inst_doa_i1_056}));
  AL_MUX \inst_doa_mux_b0/al_mux_b0_0_0  (
    .i0(inst_doa_i0_000),
    .i1(inst_doa_i1_000),
    .sel(addra_piped),
    .o(doa[0]));
  AL_MUX \inst_doa_mux_b1/al_mux_b0_0_0  (
    .i0(inst_doa_i0_001),
    .i1(inst_doa_i1_001),
    .sel(addra_piped),
    .o(doa[1]));
  AL_MUX \inst_doa_mux_b10/al_mux_b0_0_0  (
    .i0(inst_doa_i0_010),
    .i1(inst_doa_i1_010),
    .sel(addra_piped),
    .o(doa[10]));
  AL_MUX \inst_doa_mux_b11/al_mux_b0_0_0  (
    .i0(inst_doa_i0_011),
    .i1(inst_doa_i1_011),
    .sel(addra_piped),
    .o(doa[11]));
  AL_MUX \inst_doa_mux_b12/al_mux_b0_0_0  (
    .i0(inst_doa_i0_012),
    .i1(inst_doa_i1_012),
    .sel(addra_piped),
    .o(doa[12]));
  AL_MUX \inst_doa_mux_b13/al_mux_b0_0_0  (
    .i0(inst_doa_i0_013),
    .i1(inst_doa_i1_013),
    .sel(addra_piped),
    .o(doa[13]));
  AL_MUX \inst_doa_mux_b14/al_mux_b0_0_0  (
    .i0(inst_doa_i0_014),
    .i1(inst_doa_i1_014),
    .sel(addra_piped),
    .o(doa[14]));
  AL_MUX \inst_doa_mux_b15/al_mux_b0_0_0  (
    .i0(inst_doa_i0_015),
    .i1(inst_doa_i1_015),
    .sel(addra_piped),
    .o(doa[15]));
  AL_MUX \inst_doa_mux_b16/al_mux_b0_0_0  (
    .i0(inst_doa_i0_016),
    .i1(inst_doa_i1_016),
    .sel(addra_piped),
    .o(doa[16]));
  AL_MUX \inst_doa_mux_b17/al_mux_b0_0_0  (
    .i0(inst_doa_i0_017),
    .i1(inst_doa_i1_017),
    .sel(addra_piped),
    .o(doa[17]));
  AL_MUX \inst_doa_mux_b18/al_mux_b0_0_0  (
    .i0(inst_doa_i0_018),
    .i1(inst_doa_i1_018),
    .sel(addra_piped),
    .o(doa[18]));
  AL_MUX \inst_doa_mux_b19/al_mux_b0_0_0  (
    .i0(inst_doa_i0_019),
    .i1(inst_doa_i1_019),
    .sel(addra_piped),
    .o(doa[19]));
  AL_MUX \inst_doa_mux_b2/al_mux_b0_0_0  (
    .i0(inst_doa_i0_002),
    .i1(inst_doa_i1_002),
    .sel(addra_piped),
    .o(doa[2]));
  AL_MUX \inst_doa_mux_b20/al_mux_b0_0_0  (
    .i0(inst_doa_i0_020),
    .i1(inst_doa_i1_020),
    .sel(addra_piped),
    .o(doa[20]));
  AL_MUX \inst_doa_mux_b21/al_mux_b0_0_0  (
    .i0(inst_doa_i0_021),
    .i1(inst_doa_i1_021),
    .sel(addra_piped),
    .o(doa[21]));
  AL_MUX \inst_doa_mux_b22/al_mux_b0_0_0  (
    .i0(inst_doa_i0_022),
    .i1(inst_doa_i1_022),
    .sel(addra_piped),
    .o(doa[22]));
  AL_MUX \inst_doa_mux_b23/al_mux_b0_0_0  (
    .i0(inst_doa_i0_023),
    .i1(inst_doa_i1_023),
    .sel(addra_piped),
    .o(doa[23]));
  AL_MUX \inst_doa_mux_b24/al_mux_b0_0_0  (
    .i0(inst_doa_i0_024),
    .i1(inst_doa_i1_024),
    .sel(addra_piped),
    .o(doa[24]));
  AL_MUX \inst_doa_mux_b25/al_mux_b0_0_0  (
    .i0(inst_doa_i0_025),
    .i1(inst_doa_i1_025),
    .sel(addra_piped),
    .o(doa[25]));
  AL_MUX \inst_doa_mux_b26/al_mux_b0_0_0  (
    .i0(inst_doa_i0_026),
    .i1(inst_doa_i1_026),
    .sel(addra_piped),
    .o(doa[26]));
  AL_MUX \inst_doa_mux_b27/al_mux_b0_0_0  (
    .i0(inst_doa_i0_027),
    .i1(inst_doa_i1_027),
    .sel(addra_piped),
    .o(doa[27]));
  AL_MUX \inst_doa_mux_b28/al_mux_b0_0_0  (
    .i0(inst_doa_i0_028),
    .i1(inst_doa_i1_028),
    .sel(addra_piped),
    .o(doa[28]));
  AL_MUX \inst_doa_mux_b29/al_mux_b0_0_0  (
    .i0(inst_doa_i0_029),
    .i1(inst_doa_i1_029),
    .sel(addra_piped),
    .o(doa[29]));
  AL_MUX \inst_doa_mux_b3/al_mux_b0_0_0  (
    .i0(inst_doa_i0_003),
    .i1(inst_doa_i1_003),
    .sel(addra_piped),
    .o(doa[3]));
  AL_MUX \inst_doa_mux_b30/al_mux_b0_0_0  (
    .i0(inst_doa_i0_030),
    .i1(inst_doa_i1_030),
    .sel(addra_piped),
    .o(doa[30]));
  AL_MUX \inst_doa_mux_b31/al_mux_b0_0_0  (
    .i0(inst_doa_i0_031),
    .i1(inst_doa_i1_031),
    .sel(addra_piped),
    .o(doa[31]));
  AL_MUX \inst_doa_mux_b32/al_mux_b0_0_0  (
    .i0(inst_doa_i0_032),
    .i1(inst_doa_i1_032),
    .sel(addra_piped),
    .o(doa[32]));
  AL_MUX \inst_doa_mux_b33/al_mux_b0_0_0  (
    .i0(inst_doa_i0_033),
    .i1(inst_doa_i1_033),
    .sel(addra_piped),
    .o(doa[33]));
  AL_MUX \inst_doa_mux_b34/al_mux_b0_0_0  (
    .i0(inst_doa_i0_034),
    .i1(inst_doa_i1_034),
    .sel(addra_piped),
    .o(doa[34]));
  AL_MUX \inst_doa_mux_b35/al_mux_b0_0_0  (
    .i0(inst_doa_i0_035),
    .i1(inst_doa_i1_035),
    .sel(addra_piped),
    .o(doa[35]));
  AL_MUX \inst_doa_mux_b36/al_mux_b0_0_0  (
    .i0(inst_doa_i0_036),
    .i1(inst_doa_i1_036),
    .sel(addra_piped),
    .o(doa[36]));
  AL_MUX \inst_doa_mux_b37/al_mux_b0_0_0  (
    .i0(inst_doa_i0_037),
    .i1(inst_doa_i1_037),
    .sel(addra_piped),
    .o(doa[37]));
  AL_MUX \inst_doa_mux_b38/al_mux_b0_0_0  (
    .i0(inst_doa_i0_038),
    .i1(inst_doa_i1_038),
    .sel(addra_piped),
    .o(doa[38]));
  AL_MUX \inst_doa_mux_b39/al_mux_b0_0_0  (
    .i0(inst_doa_i0_039),
    .i1(inst_doa_i1_039),
    .sel(addra_piped),
    .o(doa[39]));
  AL_MUX \inst_doa_mux_b4/al_mux_b0_0_0  (
    .i0(inst_doa_i0_004),
    .i1(inst_doa_i1_004),
    .sel(addra_piped),
    .o(doa[4]));
  AL_MUX \inst_doa_mux_b40/al_mux_b0_0_0  (
    .i0(inst_doa_i0_040),
    .i1(inst_doa_i1_040),
    .sel(addra_piped),
    .o(doa[40]));
  AL_MUX \inst_doa_mux_b41/al_mux_b0_0_0  (
    .i0(inst_doa_i0_041),
    .i1(inst_doa_i1_041),
    .sel(addra_piped),
    .o(doa[41]));
  AL_MUX \inst_doa_mux_b42/al_mux_b0_0_0  (
    .i0(inst_doa_i0_042),
    .i1(inst_doa_i1_042),
    .sel(addra_piped),
    .o(doa[42]));
  AL_MUX \inst_doa_mux_b43/al_mux_b0_0_0  (
    .i0(inst_doa_i0_043),
    .i1(inst_doa_i1_043),
    .sel(addra_piped),
    .o(doa[43]));
  AL_MUX \inst_doa_mux_b44/al_mux_b0_0_0  (
    .i0(inst_doa_i0_044),
    .i1(inst_doa_i1_044),
    .sel(addra_piped),
    .o(doa[44]));
  AL_MUX \inst_doa_mux_b45/al_mux_b0_0_0  (
    .i0(inst_doa_i0_045),
    .i1(inst_doa_i1_045),
    .sel(addra_piped),
    .o(doa[45]));
  AL_MUX \inst_doa_mux_b46/al_mux_b0_0_0  (
    .i0(inst_doa_i0_046),
    .i1(inst_doa_i1_046),
    .sel(addra_piped),
    .o(doa[46]));
  AL_MUX \inst_doa_mux_b47/al_mux_b0_0_0  (
    .i0(inst_doa_i0_047),
    .i1(inst_doa_i1_047),
    .sel(addra_piped),
    .o(doa[47]));
  AL_MUX \inst_doa_mux_b48/al_mux_b0_0_0  (
    .i0(inst_doa_i0_048),
    .i1(inst_doa_i1_048),
    .sel(addra_piped),
    .o(doa[48]));
  AL_MUX \inst_doa_mux_b49/al_mux_b0_0_0  (
    .i0(inst_doa_i0_049),
    .i1(inst_doa_i1_049),
    .sel(addra_piped),
    .o(doa[49]));
  AL_MUX \inst_doa_mux_b5/al_mux_b0_0_0  (
    .i0(inst_doa_i0_005),
    .i1(inst_doa_i1_005),
    .sel(addra_piped),
    .o(doa[5]));
  AL_MUX \inst_doa_mux_b50/al_mux_b0_0_0  (
    .i0(inst_doa_i0_050),
    .i1(inst_doa_i1_050),
    .sel(addra_piped),
    .o(doa[50]));
  AL_MUX \inst_doa_mux_b51/al_mux_b0_0_0  (
    .i0(inst_doa_i0_051),
    .i1(inst_doa_i1_051),
    .sel(addra_piped),
    .o(doa[51]));
  AL_MUX \inst_doa_mux_b52/al_mux_b0_0_0  (
    .i0(inst_doa_i0_052),
    .i1(inst_doa_i1_052),
    .sel(addra_piped),
    .o(doa[52]));
  AL_MUX \inst_doa_mux_b53/al_mux_b0_0_0  (
    .i0(inst_doa_i0_053),
    .i1(inst_doa_i1_053),
    .sel(addra_piped),
    .o(doa[53]));
  AL_MUX \inst_doa_mux_b54/al_mux_b0_0_0  (
    .i0(inst_doa_i0_054),
    .i1(inst_doa_i1_054),
    .sel(addra_piped),
    .o(doa[54]));
  AL_MUX \inst_doa_mux_b55/al_mux_b0_0_0  (
    .i0(inst_doa_i0_055),
    .i1(inst_doa_i1_055),
    .sel(addra_piped),
    .o(doa[55]));
  AL_MUX \inst_doa_mux_b56/al_mux_b0_0_0  (
    .i0(inst_doa_i0_056),
    .i1(inst_doa_i1_056),
    .sel(addra_piped),
    .o(doa[56]));
  AL_MUX \inst_doa_mux_b57/al_mux_b0_0_0  (
    .i0(inst_doa_i0_057),
    .i1(inst_doa_i1_057),
    .sel(addra_piped),
    .o(doa[57]));
  AL_MUX \inst_doa_mux_b58/al_mux_b0_0_0  (
    .i0(inst_doa_i0_058),
    .i1(inst_doa_i1_058),
    .sel(addra_piped),
    .o(doa[58]));
  AL_MUX \inst_doa_mux_b59/al_mux_b0_0_0  (
    .i0(inst_doa_i0_059),
    .i1(inst_doa_i1_059),
    .sel(addra_piped),
    .o(doa[59]));
  AL_MUX \inst_doa_mux_b6/al_mux_b0_0_0  (
    .i0(inst_doa_i0_006),
    .i1(inst_doa_i1_006),
    .sel(addra_piped),
    .o(doa[6]));
  AL_MUX \inst_doa_mux_b60/al_mux_b0_0_0  (
    .i0(inst_doa_i0_060),
    .i1(inst_doa_i1_060),
    .sel(addra_piped),
    .o(doa[60]));
  AL_MUX \inst_doa_mux_b61/al_mux_b0_0_0  (
    .i0(inst_doa_i0_061),
    .i1(inst_doa_i1_061),
    .sel(addra_piped),
    .o(doa[61]));
  AL_MUX \inst_doa_mux_b62/al_mux_b0_0_0  (
    .i0(inst_doa_i0_062),
    .i1(inst_doa_i1_062),
    .sel(addra_piped),
    .o(doa[62]));
  AL_MUX \inst_doa_mux_b63/al_mux_b0_0_0  (
    .i0(inst_doa_i0_063),
    .i1(inst_doa_i1_063),
    .sel(addra_piped),
    .o(doa[63]));
  AL_MUX \inst_doa_mux_b7/al_mux_b0_0_0  (
    .i0(inst_doa_i0_007),
    .i1(inst_doa_i1_007),
    .sel(addra_piped),
    .o(doa[7]));
  AL_MUX \inst_doa_mux_b8/al_mux_b0_0_0  (
    .i0(inst_doa_i0_008),
    .i1(inst_doa_i1_008),
    .sel(addra_piped),
    .o(doa[8]));
  AL_MUX \inst_doa_mux_b9/al_mux_b0_0_0  (
    .i0(inst_doa_i0_009),
    .i1(inst_doa_i1_009),
    .sel(addra_piped),
    .o(doa[9]));

endmodule 

module AL_DFF_X
  (
  ar,
  as,
  clk,
  d,
  en,
  sr,
  ss,
  q
  );

  input ar;
  input as;
  input clk;
  input d;
  input en;
  input sr;
  input ss;
  output q;

  wire enout;
  wire srout;
  wire ssout;

  AL_MUX u_en (
    .i0(q),
    .i1(d),
    .sel(en),
    .o(enout));
  AL_MUX u_reset (
    .i0(ssout),
    .i1(1'b0),
    .sel(sr),
    .o(srout));
  AL_DFF u_seq (
    .clk(clk),
    .d(srout),
    .reset(ar),
    .set(as),
    .q(q));
  AL_MUX u_set (
    .i0(enout),
    .i1(1'b1),
    .sel(ss),
    .o(ssout));

endmodule 

module AL_MUX
  (
  input i0,
  input i1,
  input sel,
  output o
  );

  wire not_sel, sel_i0, sel_i1;
  not u0 (not_sel, sel);
  and u1 (sel_i1, sel, i1);
  and u2 (sel_i0, not_sel, i0);
  or u3 (o, sel_i1, sel_i0);

endmodule

module AL_DFF
  (
  input reset,
  input set,
  input clk,
  input d,
  output reg q
  );

  parameter INI = 1'b0;

  // synthesis translate_off
  tri0 gsrn = glbl.gsrn;

  always @(gsrn)
  begin
    if(!gsrn)
      assign q = INI;
    else
      deassign q;
  end
  // synthesis translate_on

  always @(posedge reset or posedge set or posedge clk)
  begin
    if (reset)
      q <= 1'b0;
    else if (set)
      q <= 1'b1;
    else
      q <= d;
  end

endmodule

