# lichee-tang_riscv

#### 介绍
荔枝糖FPGA的e203软核实例化

#### 软件架构
整个在linux下

#### 下载

git clone --recursive https://gitee.com/jiang_xing/lichee-tang_riscv.git

#### TD工程

prj为TD工程 td.bit可以直接烧录进fpga

引脚定义参见 constraints/lichee_tang.adc

#### openocd 编译

cd openocd
./bootstrap
./configure --disable-werror

// 打补丁主要是添加spi-flash型号
patch -p1 < ../patch/openocd.patch
make

#### 固件编译

cd firmware

make

#### 编译固件需要riscv的gcc

自己去下一个 gnu-mcu-eclipse-riscv-none-gcc
在firmware/Makefile文件修改相应目录

#### 使用荔枝糖的jtag烧录

在firmware目录中
make update
